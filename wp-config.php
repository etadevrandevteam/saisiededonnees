<?php
define('WP_AUTO_UPDATE_CORE', 'minor');// Ce paramètre est requis pour garantir le bon traitement des mises à jour WordPress dans WordPress Toolkit. Supprimez cette ligne si ce site Web WordPress n'est plus géré par WordPress Toolkit.
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'dbsaisiededonnees' );

/** MySQL database username */
define( 'DB_USER', 'usersaisie' );

/** MySQL database password */
define( 'DB_PASSWORD', 'W1uR0i7j_Y' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost:3306' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'lw6/H8+e6:rZ]l2U#&0)fEh~!zcbzuX/y-850AHL%-/dE(a4O4kg)Y(1]]]J]*3_');
define('SECURE_AUTH_KEY', 'R])E:~1Gnl+(1DD9Wx9[80fRhb*-Q~[T2sov99uvj@SVI&5En/lg)!v2]_LA4v:N');
define('LOGGED_IN_KEY', '*(98gcoLsX*@BZ5Zb6/2~]pQ56|qdxkc9k2]g-frqvAqWNSo7e;9/0vTb3|WN+)V');
define('NONCE_KEY', '5/Gs/S%!]U+157yOA(dT0Be[0vzFaf3r02h@XI6!7cY(%xCf+ah-N+18m6C0J[b7');
define('AUTH_SALT', '!0T6hmH+9W8dzz+gO#914cl|AQX3qmJ!VRP%1-Frr0UnZ9q7bX0_Id|7U-Q(7PU(');
define('SECURE_AUTH_SALT', '#p@x2:t/cT[6qn2]0*60DA*/p-uJ6u#|%64%RQ8ZGy)ua754Dyy2/+D!D/S5AJGD');
define('LOGGED_IN_SALT', '()7e4(yL06@wgPz_6&i|rqN/(*0RX(5)(S)2(1Wq%V45fy%0]845*4I_6!0w(2-/');
define('NONCE_SALT', '4m93h(1%3DL5--2d4[@ECcw%483]0k%x|7/];+1b0~D4[Bt;E6C)_![o46*37P3J');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'sd_';


define('WP_ALLOW_MULTISITE', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
